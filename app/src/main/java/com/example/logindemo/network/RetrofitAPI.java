package com.example.logindemo.network;

import com.example.logindemo.models.User;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface RetrofitAPI {
    @FormUrlEncoded
    @POST("customer/signIn")
    Call<User> login(@Header ("language") String language,@FieldMap Map<String, String> map);
}
