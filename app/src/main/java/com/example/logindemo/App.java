package com.example.logindemo;

import android.app.Application;
import android.content.Context;

import com.example.logindemo.di.component.AppComponent;
import com.example.logindemo.di.component.DaggerAppComponent;
import com.example.logindemo.di.module.AppModule;
import com.example.logindemo.di.module.ContextModule;
import com.example.logindemo.di.module.RetrofitModule;
import com.example.logindemo.di.module.UserModule;

public class App extends Application {
    private AppComponent component;

    public static App get(Context context) {
        return (App) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .contextModule(new ContextModule(this))
                .userModule(new UserModule())
                .retrofitModule(new RetrofitModule())
                .build();
    }

    public AppComponent component() {
        return component;
    }
}
