package com.example.logindemo.contract;

import com.example.logindemo.models.User;

public interface MainContract {
    interface ViewCallBack {
        void onClearText();

        void onLoginResult(Boolean result, int code);

        void onSetProgressBarVisibility(int visibility);
    }

    interface ModelCallBack {
        String getMobileNo();

        String getPassword();

        int checkUserValidity(String name, String passwd);
    }

    interface PresenterCallBack {
        void clear();

        void doLogin(User user);

        void setProgressBarVisibility(int visibility);
    }

}
