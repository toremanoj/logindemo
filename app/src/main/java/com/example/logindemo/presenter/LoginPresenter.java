package com.example.logindemo.presenter;

import androidx.lifecycle.MutableLiveData;

import com.example.logindemo.contract.MainContract;
import com.example.logindemo.models.User;
import com.example.logindemo.network.RetrofitAPI;
import com.example.logindemo.network.RetrofitClient;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresenter implements MainContract.PresenterCallBack {
    MainContract.ViewCallBack iLoginView;

    private MutableLiveData<User> userMutableLiveData = new MutableLiveData<User>();
    private boolean isLoginSuccess;
    private int code;
    RetrofitAPI retrofitAPI;

    public LoginPresenter(MainContract.ViewCallBack iLoginView,RetrofitAPI retrofitAPI) {
        this.iLoginView = iLoginView;
        this.retrofitAPI = retrofitAPI;
    }

    @Override
    public void clear() {

    }

    @Override
    public void doLogin(User user) {
        code = user.checkUserValidity(user.getMobileNo(), user.getPassword());
        if (code != 0) {
            iLoginView.onLoginResult(false, code);
        } else {
            Map<String, String> map = new HashMap<>();
            map.put("phone", user.getMobileNo());
            map.put("password", user.getPassword());
            map.put("deviceId", "wrqrqwwrqwfhghhffghfh");
            map.put("pushToken", "dsjfsifjoiwejoiwoiej");
            map.put("appVersion", "1.0");
            map.put("deviceMake", "Oppo");
            map.put("deviceModel", "A20");
            map.put("deviceType", "1");
            map.put("deviceTime", "12:45 PM");
            map.put("countryCode", "+91");
            map.put("deviceOsVersion", "10");
            map.put("loginType", "1");
            map.put("appleId", "AppleId");
            map.put("socialMediaId", "SocialId");
            Call<User> call = retrofitAPI.login("en",map);

            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    if (response.code() == 200) {
                        if (response.body() != null) {
                            userMutableLiveData.setValue(response.body());
                            isLoginSuccess = true;
                            code = response.code();
                        }
                    } else {
                        isLoginSuccess = false;
                        code = response.code();
                    }
                    final Boolean result = isLoginSuccess;
                    iLoginView.onLoginResult(result, code);
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    isLoginSuccess = false;
                    code = -1;
                    final Boolean result = isLoginSuccess;
                    iLoginView.onLoginResult(result, code);
                }
            });
        }
    }

    @Override
    public void setProgressBarVisibility(int visibility) {
        iLoginView.onSetProgressBarVisibility(visibility);
    }
}
