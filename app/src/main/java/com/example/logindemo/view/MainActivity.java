package com.example.logindemo.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.logindemo.App;
import com.example.logindemo.R;
import com.example.logindemo.contract.MainContract;
import com.example.logindemo.databinding.ActivityMainBinding;

import com.example.logindemo.di.component.DaggerActivityComponent;
import com.example.logindemo.di.module.MvpModule;
import com.example.logindemo.models.User;
import com.example.logindemo.presenter.LoginPresenter;

import javax.inject.Inject;

import static android.view.View.INVISIBLE;

public class MainActivity extends AppCompatActivity implements MainContract.ViewCallBack {
    private ActivityMainBinding binding;
    @Inject MainContract.PresenterCallBack loginPresenter;
    @Inject User user;
    @Inject
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        user = new User();
        initLoginPresenter();
        initListener();
    }

    private void initListener() {
        binding.btnLogin.setOnClickListener(v -> {
            loginPresenter.setProgressBarVisibility(View.VISIBLE);
            user.setMobileNo(binding.etMobileNumber.getText().toString());
            user.setPassword(binding.etPassword.getText().toString());
            loginPresenter.doLogin(user);
        });
    }

    private void initLoginPresenter() {
        DaggerActivityComponent.builder()
                .appComponent(App.get(this).component())
                .mvpModule(new MvpModule(this))
                .build()
                .inject(this);
        //Init login presenter
//        loginPresenter = new LoginPresenter(this);
        loginPresenter.setProgressBarVisibility(INVISIBLE);
    }

    @Override
    public void onClearText() {

    }

    @Override
    public void onLoginResult(Boolean result, int code) {
        if (result) {
            loginPresenter.setProgressBarVisibility(View.INVISIBLE);
            Toast.makeText(this, "Login Success", Toast.LENGTH_SHORT).show();
        } else {
            loginPresenter.setProgressBarVisibility(View.INVISIBLE);
            Toast.makeText(this, "Login Fail, code = " + code, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSetProgressBarVisibility(int visibility) {
        binding.progressBar.setVisibility(visibility);
    }
}