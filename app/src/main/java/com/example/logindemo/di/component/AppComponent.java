package com.example.logindemo.di.component;

import android.app.Application;
import android.content.Context;

import com.example.logindemo.App;
import com.example.logindemo.di.module.AppModule;
import com.example.logindemo.di.scope.ApplicationScope;
import com.example.logindemo.di.module.ContextModule;
import com.example.logindemo.di.module.RetrofitModule;
import com.example.logindemo.di.module.UserModule;
import com.example.logindemo.models.User;
import com.example.logindemo.network.RetrofitAPI;

import javax.inject.Singleton;

import dagger.Component;

@ApplicationScope
@Singleton
@Component(modules = {AppModule.class, UserModule.class, ContextModule.class, RetrofitModule.class})
public interface AppComponent {
    void inject(App initApplication);


    RetrofitAPI getRetrofitApi();

    Context getContext();

    User getFindItemsInteractor();

    Application getApplication();
}
