package com.example.logindemo.di.component;

import com.example.logindemo.contract.MainContract;
import com.example.logindemo.di.scope.ActivityScope;
import com.example.logindemo.di.module.MvpModule;
import com.example.logindemo.view.MainActivity;

import dagger.Component;

@ActivityScope
@Component(dependencies = AppComponent.class, modules = MvpModule.class)
public interface ActivityComponent {
    void inject(MainActivity mainActivity);

    MainContract.PresenterCallBack getMainPresenter();
}
