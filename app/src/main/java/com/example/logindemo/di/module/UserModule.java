package com.example.logindemo.di.module;

import android.graphics.ColorSpace;

import com.example.logindemo.models.User;

import dagger.Module;
import dagger.Provides;

@Module
public class UserModule {
    @Provides
    public User provideUserClass() {
        return new User();
    }
}
