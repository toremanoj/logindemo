package com.example.logindemo.di.module;

import com.example.logindemo.contract.MainContract;
import com.example.logindemo.network.RetrofitAPI;
import com.example.logindemo.presenter.LoginPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class MvpModule {
    private MainContract.ViewCallBack viewCallBack;

    public MvpModule(MainContract.ViewCallBack viewCallBack) {
        this.viewCallBack = viewCallBack;
    }

    @Provides
    public MainContract.ViewCallBack provideLoginView() {
        return viewCallBack;
    }

    @Provides
    public MainContract.PresenterCallBack provideLoginPresenter(MainContract.ViewCallBack view, RetrofitAPI retrofitAPI) {
        return new LoginPresenter(view,retrofitAPI);
    }
}
